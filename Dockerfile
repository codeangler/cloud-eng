FROM node:10.15-alpine as production

ENV NODE_ENV="production"

EXPOSE 3000

RUN apk add --no-cache tini

WORKDIR /app

COPY package*.json ./

RUN npm install --only=production && npm cache clean --force

COPY . .

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["node", "./bin/www"]

FROM production as development

ENV NODE_ENV="development"

EXPOSE 9929 3000

RUN npm install && npm cache clean --force

CMD ["../node_modules/nodemon/bin/nodemon.js", "./bin/www", "--inspect=0.0.0.0:9229"]