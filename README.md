# Coding Challenge

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Coding Challenge](#coding-challenge)
  - [The coding challenge:](#the-coding-challenge)
  - [Considerations & Decisions](#considerations--decisions)
    - [Creating New Certificates by Submission](#creating-new-certificates-by-submission)
    - [Creating & Tracking Services Certificate](#creating--tracking-services-certificate)
  - [Known Limitation](#known-limitation)
  - [Improvements & Next Steps](#improvements--next-steps)
  - [Configuration](#configuration)
    - [Create `.env`](#create-env)
  - [Production](#production)
    - [Build Docker Image for Production & Run](#build-docker-image-for-production--run)
  - [Health Check & Deployment Details](#health-check--deployment-details)
  - [Local Development](#local-development)
    - [Local Machine](#local-machine)
    - [Running Test](#running-test)
    - [Passing Test Results](#passing-test-results)
    - [Testing Known Issues](#testing-known-issues)
    - [Docker Compose [incomplete]](#docker-compose-incomplete)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## The coding challenge:

> Code should be production grade as that is how it will be evaluated - If you are time constrained, please document the additional considerations that could not be addressed in time. If you are invited to interview you will be asked to extend this code - This will give you an opportunity to understand what it would be like to work with the engineers on the team, and for our team to understand what it would be like to work with you.

> One of the services I intend on rolling out very soon is an ACME-compliant certificate service. For now, we can treat it as a stub that simply returns the string foo-${domain} as a certificate when a HTTP GET request to /cert/{domain} is made.

Implement a code base that:

- [ x ] properly handles certificates for different domains

- [ x ] allows certificates to live for 10 minutes before expiring

- [ x ] sleeps for 10 seconds when a new certificate is generated

- [ x ] generates its own certificate and keeps its certificate up-to-date when it expires

- [ x ] can generate multiple certificates at the same time

## Considerations & Decisions

### Creating New Certificates by Submission

- While the minimal criteria of returning a string-as-a-certificate `foo-${domain}` after 10 seconds is delivered, at some point the certificate needs to be validated. A discussion with product owner would be worthwhile. I've shared a few key points below.
- To Validate a issued certificate this simple string-as-a-certificate needs more information.
  - This info could be included in the certificate hash, or put on the HEADER as, for example `X-CERTIFICATE-EXPIRES` but the latter may allow for man-in-middle issues.
  - Adding the expiration to the certificate still could allow for fraudulent behaviors if the cipher is cracked.
  - Having some kind of **store** behind the server allows for confirming that the certificate presented for validation was created by this service.
- To address some of these concerns, my submission extends the simple string-as-a-certificate to `foo-${domain}+expires:${Date+Time}` and a **store** (fake DB) is available at `/cert/` to return all created certs.
  - The store would be in database and not visible to public, but for development & verification it presently is.

### Creating & Tracking Services Certificate

- the service's **local cert** is pushed to console, logs, and added to the **store**
- Given certificates expire at 10 minutes, validating the certificate is still valid every minutes may not be sufficient. The frequency of checking **local cert**

## Known Limitation

- using time to as a key in the **store** has the unit test being brittle. this would be resolved with true DB and more robust mocks.  All the unit test pass if the milliseconds line-up (yup, time constraints, pun intended!).
- Expiration was set to nearest second and not millisecond to keep unit test more robust.

## Improvements & Next Steps

- improve data model
  - find from product owner the precision required on the 10 minute expiration, and possibly round milliseconds to nearest minute.
  -
- improve logs and error handling
  - ~trace logs (e.g. Winston)~
- improve and expand unit test
  - using better stubs, mocks
  - more negative cases
  - precommit and prepush test validation
- Semantic Versioning (e.g. SemVer) but presently using pipeline ID in interim
- build out CI for testing & security scanning, build, release, and deployment
  - security hardening (e.g. Nodejs using Helmet)
  - configuration distinctions for testing vs production
  - if necessary mocking service for over the wire testing (e.g. Mountebank)
  - Solve for hostname issue in Integration Test for CI
- DB integration
  - replace timestamps with GUID (e.g. https://www.npmjs.com/package/uuid)
- Additional error handling
- Deploy behind a load balancer & reverse proxy (e.g. Nginx)
- Process management not as critical when a container uses Tini (handles safe shutdowns) and is deployed to an orchestrator (e.g. Kubernetes).
- API Documentation (e.g. open api or swagger-ui)

## Configuration

### Create `.env`

```shell
$ pwd
# /fanatics-coding-challenge/

$ cp env.example .env
## if necessary add secrets
```

## Production

Be sure to set up`.env` , see [Configuration](#configuration)

### Build Docker Image for Production & Run

```shell
## Build
$ docker build -t service:production --target production .

## Run
$ docker run -p 3001:3000 --env-file .env service:production
```

visit `localhost:3001`

## Health Check & Deployment Details

- Health Check `<host>:<port>/info/healthcheck`
- Deployment Details `<host>:<port>/info/details` TODO: use CI to update this file
- System Status `<host>:<port>/info`
-
## Local Development

Be sure to set up`.env` , see [Configuration](#configuration)

### Local Machine
Required Node >= 10

```shell
$ npm install

$ npm run start
```

### Running Test

Open new terminal

```shell
### Run Unit Test & Integration Test
$ npm run test

### Run Unit Test Only
$ npm run test:unit

### Run Integration Test Only
$ npm run test:api

### Run Select Unit Test
$ npx jest <name of file> --watch
```
### Passing Test Results

![](./docs/passing-test.png)

### Testing Known Issues

- Use of server time at the millisecond is brittle for unit testing; See [Next Steps](#next-steps) for ideas to replace timestamps with DB GUIDs

### Docker Compose [incomplete]

TODO: Improve Development from inside container with Docker Compose

Be sure to set up`.env` , see [Configuration](#configuration)

```shell

$ docker-compose up
```

visit `localhost:3001`

