'use strict';

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var winston = require('./config/winston');

var indexRouter = require('./routes/index');
var certRouter = require('./routes/cert');
var infoRouter = require('./routes/info');
require('./lib/cert-myself')();

var app = express();

app.use(morgan('combined', { stream: winston.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/cert', certRouter);
app.use('/info', infoRouter); // Service Info & Healthcheck
app.use('/crash', indexRouter); // Demonstrate error handling

// Handle 404
app.use(function(req, res) {
  res
    .status(404)
    .sendFile(path.join(
      __dirname,
      './public',
      './404.html'));
});

// Handle 500
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
