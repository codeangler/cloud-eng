'use strict';

var createCertificate = require('../create-cert').createCertificate;
var serverTimeUTC = require('../time').serverTimeUTC;
var nearestSecond = require('../time').nearestSecond;
var host = (process.env.HOST || '127.0.0.1');

function certMyself(unitTest){

  var local = createMyCert();

  if (process.env.NODE_ENV !== 'test'){

    setInterval(function(){

      var now = serverTimeUTC();

      if (local.ExpiresUTC >= now) {

        console.log('Local Cert Still Valid');

      } else {

        console.log('Local Cert Expired');

        local = createMyCert();

      }

    }, (process.env.VALIDATE_SELF_CERTIFICATE_IN_MINUTES * 60000));
  }

  return local;
}

function createMyCert(){

  var localCreateCert = createCertificate(host);
  var localCertRecord = JSON.parse(localCreateCert);
  var localCertificateString = localCertRecord.Cert;

  console.log(`New Local Certificate: ${localCertificateString} & record ${localCreateCert}`);

  return localCertRecord;
}

function delayMyCert(){
  setTimeout(certMyself, 10000);
}
module.exports = delayMyCert;
