'use strict';

var store = require('../../store');
var configExpirationInMinutes = (process.env.CERTIFICATE_TTL_IN_MINUTES || 10);
var serverTimeUTC = require('../time').serverTimeUTC;
var nearestSecond = require('../time').nearestSecond;
var fs = require('fs');

function certificateString(url){
  var certObj = JSON.parse(createCertificate(url));

  return certObj.Cert;
}

function createCertificate(url, utc = (serverTimeUTC())){
  // TODO: error handling

  var expirationTime = setExpiration(utc, configExpirationInMinutes);
  var ApproximateExpirationString = new Date(expirationTime).toUTCString();

  var certificate = {
    id: utc,
    Host: url,
    ExpiresUTC: expirationTime,
    HumanReadableExpiration: ApproximateExpirationString,
    Cert: `foo-${url}+expires-${ApproximateExpirationString}`,
  };


  var certificateJSON = JSON.stringify(certificate);

  updateStore(certificate, utc);

  return certificateJSON;
}

function updateStore(certificate, utc){

  store[(utc)] = certificate;
  var writeStore = JSON.stringify(store);

  fs.writeFileSync('./store/store.js', `'use strict';\n\nmodule.exports = ${writeStore};\n`);

  return store;
};

function setExpiration(time = (serverTimeUTC()), expirationInMinutes = configExpirationInMinutes) {
  var timeRoundedToSeconds = nearestSecond(time);
  return (timeRoundedToSeconds + (expirationInMinutes * 60000));
}

module.exports = {
  createCertificate,
  updateStore,
  setExpiration,
  certificateString,
};
