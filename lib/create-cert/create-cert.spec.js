/* eslint-disable no-undef */
'use strict';

var testCreateCertificate = require('./').createCertificate;
var testSetExpiration = require('./').setExpiration;
var testUpdateStore = require('./').updateStore;
var store = require('../../store');
var serverTimeUTC = require('../time').serverTimeUTC;
var nearestSecond = require('../time').nearestSecond;
var testCertificateString = require('./').certificateString;
var configExpirationInMinutes = (process.env.CERTIFICATE_TTL_IN_MINUTES || 10);


describe('When setting expiration time', () => {

  it('the function will add the config time to Server Time in Second', () => {
    var testTime = (Date.now());
    var testTimeSec = (nearestSecond(testTime) + (configExpirationInMinutes * 60000));
    expect(testSetExpiration()).toEqual(testTimeSec);
  });

  it('the function will not add wrong config time to Server Time in Milliseconds', () => {
    var wrongConfig = 11;
    var testTime = (Date.now() + (wrongConfig * 60000));
    var testTimeRoundedToSeconds = (nearestSecond(testTime) + (wrongConfig * 60000));
    expect(testSetExpiration()).not.toEqual(testTimeRoundedToSeconds);
  });

});

describe('When updating store', () => {

  it('function returns store with UTC as key and Cert as Value', () => {
    var utc = 123124124;
    var testCertificate = {id: utc, Host: 'url string', Expires: 12312321};
    var mockStore = {123124124: {id: utc, Host: 'url string', Expires: 12312321}};
    var output = testUpdateStore(testCertificate, utc);
    expect(output.utc).toEqual(mockStore.utc);
  });

  it('store is updated', () => {

    var utc = 123124124;
    var mockStore = {123124124: {id: utc, Host: 'url string', Expires: 12312321}};
    var testCertificate = {id: utc, Host: 'url string', Expires: 12312321};
    testUpdateStore(testCertificate, utc);
    expect(store).toEqual(expect.objectContaining(mockStore));
  });
});

describe('When creating certificate', () => {

  it('return JSON object with expiration time in seconds', () => {
    var utc = serverTimeUTC();
    var temp = testCreateCertificate('acme.com');
    var testTime = (Date.now() + (configExpirationInMinutes * 60000));
    var testTimeRoundedToSeconds = nearestSecond(testTime);
    var testTimeHumanFriendly = new Date(testTimeRoundedToSeconds).toUTCString();
    var testCertificateJSON = JSON.stringify({id: utc, Host: 'acme.com', ExpiresUTC: testTimeRoundedToSeconds, HumanReadableExpiration: testTimeHumanFriendly, Cert: `foo-acme.com+expires-${testTimeHumanFriendly}`});
    expect(temp).toEqual(testCertificateJSON);
  });

  it('return URL as string', () => {
    var testTime = (Date.now() + (configExpirationInMinutes * 60000));
    var cert = testCertificateString('acme.com');
    var testTimeHumanFriendly = new Date(testTime).toUTCString();
    expect(cert).toEqual(`foo-acme.com+expires-${testTimeHumanFriendly}`);
  });
});
