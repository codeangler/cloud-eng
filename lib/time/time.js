'use strict';

function serverTimeUTC(unitTest){
  var now = new Date();
  var serverTimeUTC = now.getTime();

  return serverTimeUTC;
}

function nearestSecond(input) {
  return (Math.floor((input) / 1000) * 1000);
}

module.exports = {serverTimeUTC, nearestSecond};
