/* eslint-disable no-undef */
'use strict';

var serverTimeUTC = require('.').serverTimeUTC;
var nearestSecond = require('.').nearestSecond;

describe('When getting the Server Time in Milliseconds', () => {

  it('the function will return current time in UTC', () => {
    var testTime = Date.now();
    var serverTime = serverTimeUTC();
    expect(serverTime).toEqual(testTime);
  });

  it('the function will not return another time, such as Jan 1, 1970', () => {
    var unixTimeZero = Date.parse('01 Jan 1970 00:00:00 GMT');
    var serverTime = serverTimeUTC();
    expect(serverTime).not.toEqual(unixTimeZero);
  });

});

describe('When getting the nearest second', () => {

  it('the function will return current time in UTC to seconds', () => {
    var serverTime = serverTimeUTC();
    var testTime = Math.floor(Date.now() / 1000) * 1000;
    var roundServerTime = nearestSecond(serverTime);
    expect(roundServerTime).toEqual(testTime);
  });

  it('the function will fail when current time in UTC to not provided at nearest second', () => {
    var serverTime = serverTimeUTC();
    var testTime = Math.floor(Date.now() / 1000) * 1000;
    expect(serverTime).not.toEqual(testTime);
  });

});
