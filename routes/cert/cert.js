'use strict';

var express = require('express');
var router = express.Router();
var certificateString = require('../../lib/create-cert').certificateString;
var store = require('../../store');

/**
 * get all certs
 */
router.get('/', function(req, res, next) {
  var temp = () => {
    return store;
  };
  var response = temp();
  return res.json(response);
});

/**
 * Get a cert
 */
router.get('/:domain', async function(req, res, next) {
  try {
    var myCert = await certificateString(req.params.domain);
    var delayInSeconds = (1000 * process.env.CERTIFICATE_CREATION_DELAY_IN_SECONDS);
    await new Promise ((resolve, reject) => setTimeout(resolve, delayInSeconds));

    return res.send(myCert);

  } catch (e) {
    next(e);
  }
});

module.exports = router;
