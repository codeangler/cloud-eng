'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

router.get('/', function(req, res, next) {
  return res.status(200).json({health: 'green', status: '200'});
});

router.get('/healthcheck', function(req, res, next) {
  return res.sendStatus(200);
});

router.get('/details', function(req, res, next) {
  return res.sendFile(path.join(
    __dirname,
    '../../public',
    'deployment-details.json'));
});

module.exports = router;
