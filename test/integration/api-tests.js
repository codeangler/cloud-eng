/* eslint-disable no-undef */
'use strict';

const request = require('supertest');
const app = require('../../app');

describe('GET /cert/:domain', function() {
  it('respond with string containing foo-{domain}', function(done) {
    request(app)
      .get('/cert/acme.com')
      .set('Accept', 'application/json')
      .expect('Content-Type', "text/html; charset=utf-8")
      .expect(200, done);
  });
});

describe('GET /cert', function() {
  it('responds with All Certs as json', function(done) {
    request(app)
      .get('/cert')
      .set('Accept', 'application/json')
      .expect('Content-Type', "application/json; charset=utf-8")
      .expect(200, done);
  });
});

describe('GET /info', function() {
  it('respond with server status', function(done) {
    request(app)
      .get('/info')
      .set('Accept', 'application/json')
      .expect('Content-Type', "application/json; charset=utf-8")
      .expect(200, done);
  });
});

describe('GET /info/details', function() {
  it('respond with deployment details', function(done) {
    request(app)
      .get('/info/details')
      .set('Accept', 'application/json')
      .expect('Content-Type', "application/json; charset=UTF-8")
      .expect(200, done);
  });
});

describe('GET /info/healthcheck', function() {
  it('respond with server healthcheck', function(done) {
    request(app)
      .get('/info/healthcheck')
      .set('Accept', 'application/json')
      .expect('Content-Type', "text/plain; charset=utf-8")
      .expect(200, done);
  });
});
